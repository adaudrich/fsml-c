﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSML
{
    class Constraints
    {
        public static bool Check()
        {
            bool success = true;

            if (!CheckParsingSuccessful()) 
            {
                if (!Program.testMode) Output.WriteLine("Parsing of FSML not successful."); 
                success = false; 
            }

            if (!CheckSingleInitialState())
            {
                if (!Program.testMode) Output.WriteLine("More or less than one initial state"); 
                success = false; 
            }

            if(!CheckDistinctStateIds())
            {
                if (!Program.testMode) Output.WriteLine("Not all state ids are unique.");
                success = false; 
            }

            if(!CheckFSMResolvable())
            {
                if (!Program.testMode) Output.WriteLine("Not all target states are resolvable."); 
                success = false; 
            }

            if (!CheckFSMDeterministic())
            {
                if (!Program.testMode) Output.WriteLine("FSM is not deterministic"); 
                success = false; 
            }

            if (!CheckFSMReachable())
            {
                if (!Program.testMode) Output.WriteLine("Not all states reachable from initial state.");
                success = false; 
            }

            if (success)
            {
                if (!Program.testMode)Output.WriteLine("All constraint checks successful");
            }
            return success;
        }

        private static bool CheckParsingSuccessful()
        {
            return (FSM.FsmStates != null || FSM.FsmStates.Count != 0);
        }

        private static bool CheckFSMReachable()
        {
            HashSet<FSMState> list = new HashSet<FSMState>();
            FSMState initial = FSM.FsmStates.Where(s => s.initial).FirstOrDefault();
            if (initial != null) list.Add(initial);

            int count = 0;
            while (count != list.Count)
            {
                count = list.Count;
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    FSMState state = list.ElementAt(i);
                    foreach (FSMTransition t in state.transitions)
                    {
                        if (!string.IsNullOrWhiteSpace(t.id))
                        {
                            FSMState fsmState = FSM.FsmStates.Where(s => s.id == t.id).FirstOrDefault();
                            if (fsmState != null) list.Add(fsmState);
                        }
                    }
                }
            }
            return (list.Count == FSM.FsmStates.Count);
        }

        private static bool CheckSingleInitialState()
        {
            return (FSM.FsmStates.Where(s => s.initial).Count() == 1);
        }

        private static bool CheckDistinctStateIds()
        {
            bool ok = true;
            foreach (FSMState state in FSM.FsmStates)
            {
                if (FSM.FsmStates.Where(s => s.id == state.id).Count() != 1)
                {
                    ok = false;
                    break;
                }
            }
            if (ok)
            {
                Console.WriteLine();
            }
            return ok;
        }

        private static bool CheckFSMResolvable()
        {
            bool ok = true;
            foreach (FSMState state in FSM.FsmStates)
            {
                foreach (FSMTransition t in state.transitions)
                {
                    if (string.IsNullOrWhiteSpace(t.id)) continue;

                    if (FSM.FsmStates.Where(s => s.id == t.id).Count() == 0)
                    {
                        ok = false;
                        break;
                    }
                }
            }
            return ok;
        }

        private static bool CheckFSMDeterministic()
        {
            bool ok = true;
            foreach (FSMState state in FSM.FsmStates)
            {
                foreach (FSMTransition transition in state.transitions)
                {
                    if (string.IsNullOrWhiteSpace(transition.id)) continue;
                    if (state.transitions.Where(t => t.input == transition.input).Count() > 1)
                    {
                        ok = false;
                        break;
                    }
                }
            }
            return ok;
        }
    }
}
