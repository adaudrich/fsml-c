﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{
    public class Pair<X,Y>
    {
        public X x;
        public Y y;
        public Pair(X x, Y y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
