﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{
    public class Handler : HandlerBase<Action>
    {
        public void handle(Action action)
        {
            if (!Program.testMode) Output.WriteLine(action.ToString());
        }
    }
}
