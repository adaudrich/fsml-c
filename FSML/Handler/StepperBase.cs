﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{
    public abstract class StepperBase<S, I, A>
    {
        protected S state;
        protected HandlerBase<A> handler;
        private Dictionary<S, Dictionary<I, Pair<A, S>>> table = new Dictionary<S, Dictionary<I, Pair<A, S>>>();

        public void add(S from, I i, A a, S to)
        {
            if (!table.ContainsKey(from))
            {
                table.Add(from, new Dictionary<I, Pair<A, S>>());
            }
            Dictionary<I, Pair<A, S>> subtable = table[from];
            Pair<A, S> pair = new Pair<A, S>(a, to);
            subtable.Add(i, pair);
        }

        public bool step(I i)
        {
            Dictionary<I, Pair<A, S>> subtable = table[state];
            if (subtable.ContainsKey(i))
            {
                Pair<A, S> pair = subtable[i];
                S from = state;
                S to = pair.y;
                if (pair.x != null)
                {
                    handler.handle(pair.x);
                }
                state = to;
                if (!Program.testMode) Output.WriteLine(string.Format("from: {0}, input: {1} , to: {2}", from, i, to));
                return true;
            }
            else
            {
                if (!Program.testMode) Output.WriteLine("Infeasible symbol in input set. State: " + state + " Input:" + i);
                return false;
            }
        }
    }
}
