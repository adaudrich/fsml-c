﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSML
{
    class StepperHelper
    {
        public static string GetInitialState()
        {
            FSMState state = FSM.FsmStates.Where(s => s.initial).FirstOrDefault();
            return (state != null) ? "State." + state.id : "State.none";
        }

        public static List<string> GetTransitions()
        {
            List<string> list = new List<string>();
            foreach (FSMState state in FSM.FsmStates)
            {
                foreach (FSMTransition transition in state.transitions)
                {
                    string from = (string.IsNullOrWhiteSpace(state.id)) ? "none" : state.id;
                    string input = (string.IsNullOrWhiteSpace(transition.input)) ? "none" : transition.input;
                    string action = (string.IsNullOrWhiteSpace(transition.action)) ? "none" : transition.action;
                    string to = (string.IsNullOrWhiteSpace(transition.id)) ? from : transition.id;
                    string text = string.Format("State.{0}, Input.{1}, Action.{2}, State.{3}", from, input, action, to);
                    list.Add(text);
                }
            }
            return list;
        }

        public static string GetStates()
        {
            HashSet<string> list = new HashSet<string>();
            foreach (FSMState state in FSM.FsmStates)
            {
                list.Add(state.id);
            }

            string result = "";
            foreach (string s in list)
            {
                result += s + ", ";
            }
            return result.TrimEnd(", ".ToCharArray());
        }

        public static string GetInputs()
        {
            HashSet<string> list = new HashSet<string>();
            foreach (FSMState state in FSM.FsmStates)
            {
                foreach (FSMTransition transition in state.transitions)
                {
                    list.Add(transition.input);
                }
            }

            string result = "";
            foreach (string s in list)
            {
                result += s + ", ";
            }
            return result.TrimEnd(", ".ToCharArray());
        }

        public static string GetActions()
        {
            HashSet<string> list = new HashSet<string>();
            foreach (FSMState state in FSM.FsmStates)
            {
                foreach (FSMTransition transition in state.transitions)
                {
                    list.Add(transition.action);
                }
            }

            string result = "";
            foreach (string s in list)
            {
                result += s + ", ";
            }
            return result.TrimEnd(", ".ToCharArray());
        }
    }
}
