﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{
    public interface HandlerBase<A>
    {
        void handle(A a);
    }
}
