﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{
    public class FSMState
    {
        public bool initial = false;
        public string id = "";
        public List<FSMTransition> transitions = new List<FSMTransition>();

        public static FSMState Create()
        {
            return new FSMState();
        }
    }
}
