grammar FSMLGrammar;

options 
{
  language=CSharp3;
}

@lexer::namespace{FSML, System}
@parser::namespace{FSML, System}

@parser::members
{
	protected const int EOF = Eof;
}

@lexer::members
{
	protected const int EOF = Eof;
	protected const int HIDDEN = Hidden;
}

@rulecatch
{
	catch (RecognitionException)
	{
		throw;
	}
}

/*
 * Parser Rules
 */
fsm : (s = state { FSM.Add($s.value); } )* EOF;

state returns [FSMState value]:  { $value = FSMState.Create(); }(j = initial { ((FSMState)$value).initial = $j.value; }) 'state' (i = id { ((FSMState)$value).id = $i.value; } )'{' (t = transition { ((FSMState)$value).transitions.Add($t.value); } )* '}';

initial returns [bool value] :	 (INITIAL { $value = true; } | WS {$value = false;})? ;

transition returns [FSMTransition value]: { $value = FSMTransition.Create(); } (j = input {((FSMTransition)$value).input = $j.value;}) ('/' a = action {((FSMTransition)$value).action = $a.value; })? ('->' i = id {((FSMTransition)$value).id = $i.value; })? ';';

id returns[string value] : t = NAME { $value=$t.text; } ;

input returns[string value] : t = NAME { $value=$t.text; } ;

action returns[string value] : t = NAME { $value=$t.text; } ;

/*
 * Lexer Rules
 */
INITIAL : 'initial';
NAME : ('a'..'z'|'A'..'Z'|'0'..'9')+ ;
WS :  (' '|'\t'|'\r'|'\n')+ {Skip();};
