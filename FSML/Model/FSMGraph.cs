﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Glee.Splines;
using Microsoft.Glee.Drawing;
using P = Microsoft.Glee.Splines.Point;
using Microsoft.Glee.GraphViewerGdi;

namespace FSML
{
    public partial class FSMGraph : UserControl
    {
        GViewer gViewer = new GViewer();

        Node startNode;
        Node endNode;
        Edge currentEdge;

        public FSMGraph()
        {
            InitializeComponent();
            this.Controls.Add(gViewer);
            gViewer.Dock = DockStyle.Fill;
        }

        public void InitGraph()
        {
            currentEdge = null;            
            CreateAndLayoutGraph();
        }

        private void CreateAndLayoutGraph()
        {
            Graph graph = new Graph("graph");            
            
            // Add nodes
            foreach (FSMState state in FSM.FsmStates)
            {
                Node n = graph.AddNode(state.id);
                n.UserData = state;
                n.Attr.Label = state.id;
                n.Attr.Shape = Shape.Ellipse;
                if (state.initial)
                {
                    n.Attr.Fillcolor = Microsoft.Glee.Drawing.Color.Gray;
                    startNode = n;
                    endNode = n;
                }
            }

            // Add edges
            foreach (FSMState state in FSM.FsmStates)
            {
                foreach (FSMTransition transition in state.transitions)
                {
                    string source = state.id;
                    string target = (string.IsNullOrWhiteSpace(transition.id))? source : transition.id;
                    string label = transition.input + ((string.IsNullOrWhiteSpace(transition.action)) ? "" : "/" + transition.action);
                    Edge e = graph.AddEdge(source, label, target);
                    e.UserData = transition;
                }
            }
            gViewer.Graph = graph;
        }

        public void Step(Input input)
        {
            List<Edge> edges = new List<Edge>(); 
            edges.AddRange(endNode.OutEdges);
            edges.AddRange(endNode.SelfEdges);

            foreach (Edge e in edges)
            {
                FSMTransition t = (FSMTransition)e.UserData;
                if (t.input == input.ToString())
                {
                    if (currentEdge != null)
                    {
                        currentEdge.Attr.Fontcolor = Microsoft.Glee.Drawing.Color.Black;
                    }
                    startNode.Attr.Fontcolor = Microsoft.Glee.Drawing.Color.Black;
                    startNode = endNode;

                    // Set new nodes
                    startNode.Attr.Fontcolor = Microsoft.Glee.Drawing.Color.Red;
                    currentEdge = e;
                    currentEdge.Attr.Fontcolor = Microsoft.Glee.Drawing.Color.Red;
                    endNode = currentEdge.TargetNode;
                    endNode.Attr.Fontcolor = Microsoft.Glee.Drawing.Color.Red;
                    break;
                }
            }
            gViewer.Invalidate();
        }
    }
}
