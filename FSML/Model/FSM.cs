﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{
    public class FSM
    {
        private static List<FSMState> fsmStates = new List<FSMState>();
        public static List<FSMState> FsmStates
        {
            get { return fsmStates; }
        }

        public static void Reset()
        {
            fsmStates.Clear();
        }

        public static void Add(FSMState state)
        {
            fsmStates.Add(state);
        }        
    }
}
