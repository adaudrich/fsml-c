﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSML
{
    public class FSMInput
    {
        private static List<Input> fsmInput = new List<Input>()
        {
            Input.ticket,
            Input.pass,
            Input.ticket,
            Input.pass,
            Input.ticket,
            Input.ticket,
            Input.pass,
            Input.pass,
            Input.ticket,
            Input.pass,
            Input.mute,
            Input.release,
            Input.ticket,
            Input.pass
        };

        public static List<Input> FsmInput
        {
            get { return fsmInput; }
        }

        public static void Reset()
        {
            fsmInput.Clear();
        }

        public static void Add(string value)
        {
            Input input = Input.none;
            if (Enum.TryParse<Input>(value, out input))
            {
                fsmInput.Add(input);
            }
            else
            {
                throw new IllegalInputExecption("Illegal input detected. Parsing failed.");
            }
        }

        public static bool Check()
        {
            bool isStepSuccessful = true;
            Handler handler = new Handler();
            Stepper stepper = new Stepper(handler);
            foreach (Input input in FSMInput.FsmInput)
            {
                isStepSuccessful &= stepper.step(input);
            }
            return isStepSuccessful;
        }
    }
}
