grammar FSMInput;

options 
{
  language=CSharp3;
}

@lexer::namespace{FSML}
@parser::namespace{FSML}

@parser::members
{
	protected const int EOF = Eof;
}

@lexer::members
{
	protected const int EOF = Eof;
	protected const int HIDDEN = Hidden;
}

@rulecatch
{
	catch (RecognitionException)
	{
		throw;
	}
}

/*
 * Parser Rules
 */
inputs : '[' (i = input { FSMInput.Add($i.value); })? (',' s = input { FSMInput.Add($s.value); } )* ']' '.' EOF;
input returns[string value] : t = NAME { $value=$t.text; } ;

/*
 * Lexer Rules
 */
NAME : ('a'..'z'|'A'..'Z'|'0'..'9')+ ;
WS :  (' '|'\t'|'\r'|'\n')+ {Skip();};
