﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSML
{
    public partial class OutputForm : Form
    {
        public OutputForm()
        {
            InitializeComponent();
        }

        public void WriteLine(string message)
        {
            list.Items.Add(message);
            list.Invalidate();
            Application.DoEvents();
        }

        public void Update(int count, bool correct)
        {
            list.BeginUpdate();
            string failureString = list.Items[list.Items.Count-1].ToString();
            list.Items.RemoveAt(list.Items.Count-1);

            string correctString = list.Items[list.Items.Count - 1].ToString();
            list.Items.RemoveAt(list.Items.Count - 1);

            if (correct)
            {
                list.Items.Add("-> correct: " + count);
                list.Items.Add(failureString);
            }
            else
            {
                list.Items.Add(correctString);
                list.Items.Add("-> failed: " + count);
            }
            list.EndUpdate();
            Application.DoEvents();
        }
    }
}
