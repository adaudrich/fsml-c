﻿namespace FSML
{
    partial class FSMLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartDemo = new System.Windows.Forms.Button();
            this.fsmGraph = new FSML.FSMGraph();
            this.btnResetDemo = new System.Windows.Forms.Button();
            this.tbxDelay = new System.Windows.Forms.NumericUpDown();
            this.lblDelay = new System.Windows.Forms.Label();
            this.btnLoadInput = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbxDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStartDemo
            // 
            this.btnStartDemo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStartDemo.Location = new System.Drawing.Point(93, 347);
            this.btnStartDemo.Name = "btnStartDemo";
            this.btnStartDemo.Size = new System.Drawing.Size(75, 23);
            this.btnStartDemo.TabIndex = 4;
            this.btnStartDemo.Text = "Start Demo";
            this.btnStartDemo.UseVisualStyleBackColor = true;
            this.btnStartDemo.Click += new System.EventHandler(this.btnStartDemo_Click);
            // 
            // fsmGraph
            // 
            this.fsmGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fsmGraph.Location = new System.Drawing.Point(12, 12);
            this.fsmGraph.Name = "fsmGraph";
            this.fsmGraph.Size = new System.Drawing.Size(520, 329);
            this.fsmGraph.TabIndex = 5;
            // 
            // btnResetDemo
            // 
            this.btnResetDemo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnResetDemo.Location = new System.Drawing.Point(277, 347);
            this.btnResetDemo.Name = "btnResetDemo";
            this.btnResetDemo.Size = new System.Drawing.Size(75, 23);
            this.btnResetDemo.TabIndex = 6;
            this.btnResetDemo.Text = "Reset Demo";
            this.btnResetDemo.UseVisualStyleBackColor = true;
            this.btnResetDemo.Click += new System.EventHandler(this.btnResetDemo_Click);
            // 
            // tbxDelay
            // 
            this.tbxDelay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbxDelay.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.tbxDelay.Location = new System.Drawing.Point(214, 350);
            this.tbxDelay.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.tbxDelay.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.tbxDelay.Name = "tbxDelay";
            this.tbxDelay.Size = new System.Drawing.Size(57, 20);
            this.tbxDelay.TabIndex = 7;
            this.tbxDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbxDelay.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.lblDelay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblDelay.AutoSize = true;
            this.lblDelay.Location = new System.Drawing.Point(174, 352);
            this.lblDelay.Name = "label1";
            this.lblDelay.Size = new System.Drawing.Size(34, 13);
            this.lblDelay.TabIndex = 8;
            this.lblDelay.Text = "Delay";
            // 
            // btnLoadInput
            // 
            this.btnLoadInput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadInput.Location = new System.Drawing.Point(12, 347);
            this.btnLoadInput.Name = "btnLoadInput";
            this.btnLoadInput.Size = new System.Drawing.Size(75, 23);
            this.btnLoadInput.TabIndex = 9;
            this.btnLoadInput.Text = "Load Input";
            this.btnLoadInput.UseVisualStyleBackColor = true;
            this.btnLoadInput.Click += new System.EventHandler(this.btnLoadInput_Click);
            // 
            // FSMLForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 382);
            this.Controls.Add(this.btnLoadInput);
            this.Controls.Add(this.lblDelay);
            this.Controls.Add(this.tbxDelay);
            this.Controls.Add(this.btnResetDemo);
            this.Controls.Add(this.fsmGraph);
            this.Controls.Add(this.btnStartDemo);
            this.Name = "FSMLForm";
            this.Text = "FSML";
            ((System.ComponentModel.ISupportInitialize)(this.tbxDelay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartDemo;
        private FSMGraph fsmGraph;
        private System.Windows.Forms.Button btnResetDemo;
        private System.Windows.Forms.NumericUpDown tbxDelay;
        private System.Windows.Forms.Label lblDelay;
        private System.Windows.Forms.Button btnLoadInput;
    }
}

