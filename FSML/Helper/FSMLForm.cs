﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace FSML
{
    public partial class FSMLForm : Form
    {
        public FSMLForm()
        {
            InitializeComponent();
        }

        public void InitGraph()
        {
            fsmGraph.InitGraph();
        }

        public bool ShowControls 
        {
            set
            {
                btnLoadInput.Visible = value;
                btnResetDemo.Visible = value;
                btnStartDemo.Visible = value;
                tbxDelay.Visible = value;
                lblDelay.Visible = value;
            }
        }

        #region Demo
        private void btnLoadInput_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = Application.StartupPath;
            dialog.DefaultExt = ".input";
            dialog.Filter = "Finite State Machine Input (.input)|*.input";
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    LoadInput(dialog.FileName);
                    FSMInput.Check();
                }
                catch (Exception ex)
                {
                    Output.WriteLine(ex.Message);
                    FSMInput.Reset();
                }
            }
        }

        private void LoadInput(string path)
        {
            FileInfo file = new FileInfo(path);
            FSMInput.Reset();
            AntlrFileStream input = new AntlrFileStream(path);
            FSMInputLexer lexer = new FSMInputLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            FSMInputParser parser = new FSMInputParser(tokens);
            parser.inputs();
            Output.WriteLine(file.Name + " loaded");
            Output.WriteLine("Input parsed");
        }        

        private void btnStartDemo_Click(object sender, EventArgs e)
        {
            Handler handler = new Handler();
            Stepper stepper = new Stepper(handler);

            Output.WriteLine("Demo...");
            foreach (Input input in FSMInput.FsmInput)
            {
                stepper.step(input);
                fsmGraph.Step(input);
                Application.DoEvents();
                Thread.Sleep(int.Parse(tbxDelay.Value.ToString()));
            }
            fsmGraph.InitGraph();
        }

        private void btnResetDemo_Click(object sender, EventArgs e)
        {
            if (FSM.FsmStates.Count > 0)
            {
                fsmGraph.InitGraph();
            }
        }
        #endregion

    }
}
