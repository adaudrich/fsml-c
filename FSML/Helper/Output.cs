﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSML
{
    public class Output
    {
        static OutputForm form;
        public static void WriteLine(string message)
        {
            if (form == null)
            {
                form = new OutputForm();
                form.Show();
            }
            form.WriteLine(message);
        }

        public static void Update(int count, bool correct)
        {
            form.Update(count, correct);
        }
    }
}
