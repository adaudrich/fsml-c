﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FSML
{
    class FSMLHandler
    {
        private List<string> paths = new List<string>()
        {
            //"tests\\determinismNotOk.fsml",
            //"tests\\idsNotOk.fsml",
            //"tests\\initialNotOk.fsml",
            //"tests\\parserError.fsml",
            //"tests\\reachabilityNotOk.fsml",
            //"tests\\resolutionNotOk.fsml",
            "tests\\sample.fsml",
        };
        public void Show()
        {
            // Cycle through fsmls
            foreach (string path in paths)
            {
                if (ParseFsml(path)) { ShowGraph(); }
            }
        }

        public void ShowGraph()
        {
            FSMLForm form = new FSMLForm();
            form.InitGraph();
            form.ShowDialog();
        }

        #region Parser
        public static bool ParseFsml(string path)
        {
            if (LoadGrammar(path) && Constraints.Check())
            {
                GenerateCode();
                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool LoadGrammar(string path)
        {
            FileInfo file = new FileInfo(path);
            FSM.Reset();
            AntlrFileStream input = new AntlrFileStream(path);
            FSMLGrammarLexer lexer = new FSMLGrammarLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            FSMLGrammarParser parser = new FSMLGrammarParser(tokens);            
            parser.fsm();
            if (parser.NumberOfSyntaxErrors > 0)
            {
                Output.WriteLine(parser.NumberOfSyntaxErrors + " syntax errors detected. Parsing failed.");
                return false;
            }
            Output.WriteLine(file.Name + " loaded");
            Output.WriteLine("Language parsed");
            return true;
        }

        #region Generate Code
        private static void GenerateCode()
        {
            GenerateStates();
            GenerateInputs();
            GenerateActions();
            GenerateStepper();
        }

        private static void GenerateStates()
        {
            StateEnumTemplate stateEnumTemplate = new StateEnumTemplate();
            String content = stateEnumTemplate.TransformText();
            File.WriteAllText(Application.StartupPath + @"\..\..\GeneratedCode\State.cs", content);
        }

        private static void GenerateInputs()
        {
            InputEnumTemplate inputEnumTemplate = new InputEnumTemplate();
            String content = inputEnumTemplate.TransformText();
            File.WriteAllText(Application.StartupPath + @"\..\..\GeneratedCode\Input.cs", content);
        }

        private static void GenerateActions()
        {
            ActionEnumTemplate actionEnumTemplate = new ActionEnumTemplate();
            String content = actionEnumTemplate.TransformText();
            File.WriteAllText(Application.StartupPath + @"\..\..\GeneratedCode\Action.cs", content);
        }

        private static void GenerateStepper()
        {
            StepperTemplate stepperTemplate = new StepperTemplate();
            String content = stepperTemplate.TransformText();
            File.WriteAllText(Application.StartupPath + @"\..\..\GeneratedCode\Stepper.cs", content);
        }
        #endregion
        #endregion
    }
}
