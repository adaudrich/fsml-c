﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FSML
{

    class FSMInputTestHandler
    {
        #region Properties
        Random random = new Random();

        List<string> correctTests = new List<string>();
        Dictionary<List<string>, FSMState> correctInputs = new Dictionary<List<string>, FSMState>();

        List<string> incorrectTests = new List<string>();
        List<List<string>> inCorrectInputs = new List<List<string>>();

        List<string> parserTests = new List<string>();
        List<List<string>> parserInputs = new List<List<string>>();
        #endregion

        public FSMInputTestHandler()
        {
            FSMLHandler.ParseFsml("sample.fsml");
        }

        public void DoTests(int depth)
        {
            DateTime start = DateTime.Now;
            GenerateTestCases(depth);
            Test();
            Output.WriteLine("Input Tests - Count: " + (correctTests.Count + incorrectTests.Count + parserTests.Count) + " Depth: " + depth + " - " + (DateTime.Now - start));
        }

        #region Generate Tests
        /*
        * 4 cases:
        * 1) correct input token in correct state
        * 2) correct input token but wrong state
        * 3) wrong input token ("foo", "bar")
        * 4) parser error in input stream ("[a,b,c,,].", "a,b,c].")
        */
        private void GenerateTestCases( int depth)
        {
            FSMState start = FSM.FsmStates.Where(s => s.initial).FirstOrDefault();
            correctInputs.Add(new List<string>(), start);

            for (int i = 0; i < depth; i++)
            {
                GenerateInputList();
            }

            GenerateCorrectInputTests();
            GenerateIncorrectInputTests(depth);
            GenerateParserInputTests(depth);
        }

        private void GenerateInputList()
        {
            Dictionary<List<string>, FSMState> inputs = correctInputs;
            correctInputs = new Dictionary<List<string>, FSMState>();

            foreach (List<string> list in inputs.Keys)
            {
                List<string> inputPool = Enum.GetNames(typeof(Input)).ToList();
                inputPool.Remove("none");                

                FSMState state = inputs[list];
                foreach (FSMTransition transition in state.transitions)
                {
                    inputPool.Remove(transition.input);

                    // Generate correct input
                    FSMState nextState = (transition.id == "") ? state : FSM.FsmStates.Where(s => s.id == transition.id).FirstOrDefault();
                    List<string> newList = new List<string>();
                    newList.AddRange(list);
                    newList.Add(transition.input);
                    correctInputs.Add(newList, nextState);

                    // Generate parser error input
                    List<string> parserList = new List<string>();
                    parserList.AddRange(list);
                    int start = random.Next(1,transition.input.Length-1);
                    int count = random.Next(1, transition.input.Length - start);
                    string parserError = transition.input.Remove(start, count);
                    parserList.Add(parserError);
                    parserInputs.Add(parserList);
                }

                // Generate infeasible input
                foreach (string input in inputPool)
                {
                    List<string> infeasibleInputList = new List<string>();
                    infeasibleInputList.AddRange(list);
                    infeasibleInputList.Add(input);
                    inCorrectInputs.Add(infeasibleInputList);
                }

                // Generate illegal input
                List<string> illegalInputList = new List<string>();
                illegalInputList.AddRange(list);
                illegalInputList.Add("foo");
                inCorrectInputs.Add(illegalInputList);
            }
        }
        
        private void GenerateCorrectInputTests()
        {
            Output.WriteLine("Generate correct input tests");
            foreach (List<string> list in correctInputs.Keys)
            {
                string text = "[";
                for (int i = 0; i < list.Count; i++)
                {
                    text += list[i];
                    text += (i == list.Count - 1) ? "]." : ", ";
                }
                correctTests.Add(text);
            }
        }

        private void GenerateIncorrectInputTests(int depth)
        {
            Output.WriteLine("Generate failure input tests");
            List<string> inputs = Enum.GetNames(typeof(Input)).ToList();
            inputs.Remove("none");

            for (int i = 0; i < inCorrectInputs.Count; i++)
            {
                List<string> list = inCorrectInputs[i];
                while (list.Count <= depth)
                {
                    list.Add(inputs[random.Next(inputs.Count)]);
                }
                string text = "[";
                for (int j = 0; j < list.Count; j++)
                {
                    text += list[j];
                    text += (j == list.Count - 1) ? "]." : ", ";
                }
                incorrectTests.Add(text);
            }
        }

        private void GenerateParserInputTests(int depth)
        {
            Output.WriteLine("Generate parser error input tests");
            List<string> inputs = Enum.GetNames(typeof(Input)).ToList();
            inputs.Remove("none");

            for (int i = 0; i < parserInputs.Count; i++)
            {
                List<string> list = parserInputs[i];
                while (list.Count <= depth)
                {
                    list.Add(inputs[random.Next(inputs.Count)]);
                }
                string text = "[";
                for (int j = 0; j < list.Count; j++)
                {
                    text += list[j];
                    text += (j == list.Count - 1) ? "]." : ", ";
                }
                parserTests.Add(text);
            }
        }
        #endregion

        #region Test
        private void Test()
        {
            TestCorrectTestCases();
            TestFailureTestCases();
            TestParserTestCases();
        }

        private void TestCorrectTestCases()
        {
            int correct = 0;
            int failed = 0;
            Output.WriteLine("Test correct input test cases - count: " + correctTests.Count);
            Output.WriteLine("-> correct: " + correct);
            Output.WriteLine("-> failed: " + failed);

            foreach (string testInput in correctTests)
            {
                if (ParserTest(testInput) && FSMInput.Check())
                {
                    correct++;
                    Output.Update(correct, true);
                }
                else
                {                    
                    failed++;
                    Output.Update(failed, false);
                }
            }
        }

        private void TestFailureTestCases()
        {
            int correct = 0;
            int failed = 0;
            Output.WriteLine("Test failure input test cases - count: " + incorrectTests.Count);
            Output.WriteLine("-> correct: " + correct);
            Output.WriteLine("-> failed: " + failed);

            foreach (string testInput in incorrectTests)
            {
                if ( ParserTest(testInput) && FSMInput.Check())
                {
                    failed++;
                    Output.Update(failed, false);
                }
                else
                {
                    correct++;
                    Output.Update(correct, true);
                }
            }
        }

        private void TestParserTestCases()
        {
            int correct = 0;
            int failed = 0;
            Output.WriteLine("Test parser failure input test cases - count: " + parserTests.Count);
            Output.WriteLine("-> correct: " + correct);
            Output.WriteLine("-> failed: " + failed);

            foreach (string testInput in parserTests)
            {
                if (ParserTest(testInput) && FSMInput.Check())
                {
                    failed++;
                    Output.Update(failed, false);
                }
                else
                {
                    correct++;
                    Output.Update(correct, true);
                }
            }
        }

        private bool ParserTest(string testInput)
        {
            FSMInput.Reset();
            AntlrInputStream input = new AntlrInputStream(testInput);
            FSMInputLexer lexer = new FSMInputLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            FSMInputParser parser = new FSMInputParser(tokens);
            try
            {
                parser.inputs();
            }
            catch { return false; }
            return parser.NumberOfSyntaxErrors == 0;
        }
        #endregion
    }
}
