﻿using Antlr4.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSML
{
    class FSMLanguageTestHandler
    {
        #region Properties
        Random random = new Random();
        List<List<string>[]> languageItemPools = new List<List<string>[]>();
        List<string> correctLanguages = new List<string>();
        List<string> incorrectLanguages = new List<string>();
        #endregion

        public void DoTests(int depth)
        {
            DateTime start = DateTime.Now;
            GenerateTestCases(depth);
            Test();
            Output.WriteLine("Language Tests - Count: " + (correctLanguages.Count + incorrectLanguages.Count) + " Depth: " + depth + " - " + (DateTime.Now - start));
        }

        #region Generate Tests
        private void GenerateTestCases(int depth)
        {
            GenerateLanguageComponents(depth);

            // correct languages
            foreach (List<string>[] pool in languageItemPools)
            {
                string language = GenerateCorrectLanguages(pool);
                correctLanguages.Add(language);
            }

            // incorrect languages
            foreach (string language in correctLanguages)
            {
                List<string> languages = GenerateIncorrectLanguages(language);
                incorrectLanguages.AddRange(languages);
            }
        }

        #region Generate Language Items
        private void GenerateLanguageComponents(int depth)
        {
            for (int i = 0; i < depth; i++)
            {
                List<string>[] itemPool = new List<string>[3];
                itemPool[0] = GenerateItemPool(depth);
                itemPool[1] = GenerateItemPool(depth);
                itemPool[2] = GenerateItemPool(depth);
                languageItemPools.Add(itemPool);
            }
        }

        private List<string> GenerateItemPool(int depth)
        {
            // assert: ids are unique
            HashSet<string> items = new HashSet<string>();
            string chars = "abcdefghijklmnopqrstuvwxyz";
            int count = random.Next(2, depth);
            for (int i = 0; i < count; i++)
            {
                int length = random.Next(2, 10);
                string randomItem = new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
                items.Add(randomItem);
            }
            return items.ToList();
        }
        #endregion

        #region Correct Languages
        private string GenerateCorrectLanguages(List<string>[] itemPool)
        {
            string language = "";
            Dictionary<string, string> states = GenerateStates(itemPool[0]);
            List<string> transitions = GenerateTransitions(states, itemPool[1], itemPool[2]);
            foreach (string transition in transitions)
            {
                language += transition;
            }
            return language;
        }

        private Dictionary<string, string> GenerateStates(List<string> pool)
        {
            // assert: only one initial state
            Dictionary<string, string> states = new Dictionary<string, string>();
            string initialState = pool[random.Next(pool.Count)];
            states.Add(initialState, "initial state " + initialState);
            foreach (string state in pool)
            {
                if (state != initialState)
                {
                    states.Add(state, "state " + state);
                }
            }
            return states;
        }

        private List<string> GenerateTransitions(Dictionary<string, string> states, List<string> inputs, List<string> actions)
        {
            List<string> transitionsTemp = new List<string>();
            List<string> transitions = new List<string>();
            List<string> statesPool = new List<string>(states.Keys);
            string initialState = states.Where(p => p.Value.StartsWith("initial")).FirstOrDefault().Key;
            string lastState = initialState;

            // assert: all states are reachable and resolvable
            while (statesPool.Count > 0)
            {
                string transition = "";
                string action = "/" + actions[random.Next(actions.Count)];
                bool isAction = (random.Next() % 4) != 0;
                string input = inputs[random.Next(inputs.Count)];
                input = (isAction) ? input + action : input;

                statesPool.Remove(lastState);
                if (statesPool.Count == 0)
                {
                    // if last state, go to initial state
                    transition = states[lastState] + " { " + string.Format("{0} -> {1}; ", input, initialState);
                }
                else
                {
                    // go to random next state
                    string nextState = statesPool[random.Next(statesPool.Count)];
                    transition = states[lastState] + " { " + string.Format("{0} -> {1}; ", input, nextState);
                    lastState = nextState;
                }
                transitionsTemp.Add(transition);
            }

            // fill with other random transitions
            statesPool = new List<string>(states.Keys);
            for (int i = 0; i < transitionsTemp.Count; i++)
            {
                int count = random.Next(inputs.Count);
                string transition = transitionsTemp[i];
                for (int j = 0; j < count; j++)
                {
                    string nextState = statesPool[random.Next(statesPool.Count)];
                    string action = "/" + actions[random.Next(actions.Count)];
                    bool isAction = (random.Next() % 4) != 0;
                    string input = inputs[random.Next(inputs.Count)];

                    // assert: language is deterministic
                    if (transition.Contains(input)
                        || transition.Contains(action)
                        || (transition.Contains(nextState) && !transition.Contains("state " + nextState)))
                    {
                        continue;
                    }
                    input = (isAction) ? input + action : input;

                    // transition to self, needs only input
                    if (transition.Contains("state " + nextState))
                    {
                        transition += string.Format("{0}; ", input);
                    }
                    else
                    {
                        transition += string.Format("{0} -> {1}; ", input, nextState);
                    }
                }
                transition += "} ";
                transitions.Add(transition);
            }
            return transitions;
        }
        #endregion

        #region Incorrect Languages
        private List<string> GenerateIncorrectLanguages(string language)
        {
            List<string> languages = new List<string>();
            languages.Add(DuplicateStates(language));
            languages.Add(NoInitial(language));
            languages.Add(MoreThanOneInitial(language));
            languages.Add(NotResolvableState(language));
            languages.Add(NotDeterministicLanguage(language));
            languages.Add(NotReachableLanguage(language));
            return languages;
        }

        private string DuplicateStates(string language)
        {
            string[] tokens = language.Split(' ');

            string head = "";
            List<string> heads = new List<string>();

            string body = "";
            bool isBody = false;
            List<string> bodies = new List<string>();

            foreach (string token in tokens)
            {
                if (token == "{")
                {
                    isBody = true;
                    heads.Add(head);
                    head = "";
                }

                if (isBody)
                {
                    body += token + " ";
                }
                else
                {
                    head += token + " ";
                }

                if (token == "}")
                {
                    isBody = false;
                    bodies.Add(body);
                    body = "";
                }
            }
            string duplicateHead = heads[random.Next(heads.Count)].Replace("initial ", "");
            string randomBody = bodies[random.Next(bodies.Count)];
            language += duplicateHead + randomBody;
            return language;
        }

        private string NoInitial(string language)
        {
            return language.Replace("initial ", "");
        }

        private string MoreThanOneInitial(string language)
        {
            language = language.Replace("state", "initial state");
            language = language.Replace("initial initial state", "initial state");
            return language;
        }

        private string NotResolvableState(string language)
        {
            List<string> states = GetStates(language);
            string unresolvableState = states[random.Next(states.Count)];
            while (!language.Contains("-> " + unresolvableState))
            {
                unresolvableState = states[random.Next(states.Count)];
            }
            language = language.Replace("-> " + unresolvableState, "-> " + "-> " + unresolvableState + "_us");
            return language;
        }
        
        private string NotReachableLanguage(string language)
        {
            List<string> states = GetStates(language);
            string unreachableState = states[random.Next(states.Count)];
            while (language.Contains("initial state " + unreachableState))
            {
                unreachableState = states[random.Next(states.Count)];
            }
            language = language.Replace(" -> " + unreachableState, "");
            return language;
        }

        private string NotDeterministicLanguage(string language)
        {
            List<string> states = GetStates(language);

            List<string> transitions = new List<string>();
            bool isNextTransition = false;
            string transition = "";

            string[] tokens = language.Split(' ');
            foreach (string token in tokens)
            {
                if (token == "}")
                {
                    isNextTransition = false;
                }
                if (isNextTransition)
                {
                    if (token.EndsWith(";"))
                    {
                        transition += token;
                        transitions.Add(transition);
                        transition = "";
                    }
                    else
                    {
                        transition += token + " ";
                    }
                }
                if (token == "{")
                {
                    isNextTransition = true;
                }
            }

            string nonDeterministicTransition = transitions[random.Next(transitions.Count)];
            while (!nonDeterministicTransition.Contains("->"))
            {
                nonDeterministicTransition = transitions[random.Next(transitions.Count)];
            }
            string replaceTransition = nonDeterministicTransition;
            string replaceState = states.Where(s => replaceTransition.Contains("-> " + s)).FirstOrDefault();
            replaceTransition = replaceTransition.Replace(replaceState, states.Where(s => s != replaceState).FirstOrDefault());
            language = language.Replace(nonDeterministicTransition, replaceTransition + " " + nonDeterministicTransition);
            return language;
        }
        
        private List<string> GetStates(string language)
        {
            List<string> states = new List<string>();
            string[] tokens = language.Split(' ');
            bool isNextAState = false;
            foreach (string token in tokens)
            {
                if (isNextAState)
                {
                    states.Add(token);
                }
                isNextAState = (token == "state");
            }
            return states;
        }
        #endregion
        #endregion

        #region Test
        private void Test()
        {
            TestCorrectTestCases();
            TestIncorrectTestCases();
        }

        private void TestIncorrectTestCases()
        {
            int correct = 0;
            int failed = 0;
            Output.WriteLine("Test incorrect language test cases - count: " + incorrectLanguages.Count);
            Output.WriteLine("-> correct: " + correct);
            Output.WriteLine("-> failed: " + failed);
            foreach (string testInput in incorrectLanguages)
            {
                if (LoadGrammar(testInput) && Constraints.Check())
                {
                    failed++;
                    Output.Update(failed, false);
                }
                else
                {
                    correct++;
                    Output.Update(correct, true);
                }
            }
        }

        private void TestCorrectTestCases()
        {
            int correct = 0;
            int failed = 0;
            Output.WriteLine("Test correct language test cases - count: " + correctLanguages.Count);
            Output.WriteLine("-> correct: " + correct);
            Output.WriteLine("-> failed: " + failed);
            foreach (string testInput in correctLanguages)
            {
                if (LoadGrammar(testInput) && Constraints.Check())
                {
                    correct++;
                    Output.Update(correct, true);
                }
                else
                {
                    failed++;
                    Output.Update(failed, false);
                }
            }
        }

        private static bool LoadGrammar(string testInput)
        {
            FSM.Reset();
            AntlrInputStream input = new AntlrInputStream(testInput);
            FSMLGrammarLexer lexer = new FSMLGrammarLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            FSMLGrammarParser parser = new FSMLGrammarParser(tokens);
            parser.fsm();
            if (parser.NumberOfSyntaxErrors > 0)
            {
                return false;
            }

            if (Program.showGraphs)
            {
                FSMLForm form = new FSMLForm();
                form.ShowControls = false;
                form.InitGraph();
                form.ShowDialog();
            }
            return true;
        }
        #endregion
    }
}
