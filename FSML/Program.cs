﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FSML
{    
    static class Program
    {
        public static bool testMode = false;
        public static bool showGraphs = false;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            // Assignment 1
            //FSMLHandler handler = new FSMLHandler();
            //handler.Show();
            
            // Assignment 2
            // Input Tests
            testMode = true;
            //FSMInputTestHandler inputTests = new FSMInputTestHandler();
            //inputTests.DoTests(7);            

            // Language Tests
            FSMLanguageTestHandler languageTests = new FSMLanguageTestHandler();
            //showGraphs = true;
            languageTests.DoTests(100);

            MessageBox.Show("");
        }
    }
}
