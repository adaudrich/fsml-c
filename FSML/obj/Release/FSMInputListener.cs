//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.1-SNAPSHOT
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from D:\projects\csharp\FSML\FSML\Model\FSMInput.g4 by ANTLR 4.1-SNAPSHOT
namespace FSML {
using Antlr4.Runtime.Misc;
using IParseTreeListener = Antlr4.Runtime.Tree.IParseTreeListener;
using IToken = Antlr4.Runtime.IToken;

/// <summary>
/// This interface defines a complete listener for a parse tree produced by
/// <see cref="FSMInputParser"/>.
/// </summary>
[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.1-SNAPSHOT")]
public interface IFSMInputListener : IParseTreeListener {
	/// <summary>
	/// Enter a parse tree produced by <see cref="FSMInputParser.input"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	void EnterInput([NotNull] FSMInputParser.InputContext context);
	/// <summary>
	/// Exit a parse tree produced by <see cref="FSMInputParser.input"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	void ExitInput([NotNull] FSMInputParser.InputContext context);

	/// <summary>
	/// Enter a parse tree produced by <see cref="FSMInputParser.inputs"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	void EnterInputs([NotNull] FSMInputParser.InputsContext context);
	/// <summary>
	/// Exit a parse tree produced by <see cref="FSMInputParser.inputs"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	void ExitInputs([NotNull] FSMInputParser.InputsContext context);
}
} // namespace FSML
