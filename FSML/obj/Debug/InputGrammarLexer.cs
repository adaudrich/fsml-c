//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.1-SNAPSHOT
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from D:\projects\csharp\FSML\FSML\InputGrammar.g4 by ANTLR 4.1-SNAPSHOT
namespace FSML {
using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Misc;
using DFA = Antlr4.Runtime.Dfa.DFA;

[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.1-SNAPSHOT")]
public partial class InputGrammarLexer : Lexer {
	public const int
		T__5=1, T__4=2, T__3=3, T__2=4, T__1=5, T__0=6, INITIAL=7, NAME=8, WS=9;
	public static string[] modeNames = {
		"DEFAULT_MODE"
	};

	public static readonly string[] tokenNames = {
		"<INVALID>",
		"'->'", "'{'", "'/'", "'}'", "';'", "'state'", "'initial'", "NAME", "WS"
	};
	public static readonly string[] ruleNames = {
		"T__5", "T__4", "T__3", "T__2", "T__1", "T__0", "INITIAL", "NAME", "WS"
	};


		protected const int EOF = Eof;
		protected const int HIDDEN = Hidden;


	public InputGrammarLexer(ICharStream input)
		: base(input)
	{
		_interp = new LexerATNSimulator(this,_ATN);
	}

	public override string GrammarFileName { get { return "InputGrammar.g4"; } }

	public override string[] TokenNames { get { return tokenNames; } }

	public override string[] RuleNames { get { return ruleNames; } }

	public override string[] ModeNames { get { return modeNames; } }

	public override void Action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 8 : WS_action(_localctx, actionIndex); break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: Skip(); break;
		}
	}

	public static readonly string _serializedATN =
		"\x3\xB6D5\x5D61\xF22C\xAD89\x44D2\xDF97\x846A\xE419\x2\v:\b\x1\x4\x2\t"+
		"\x2\x4\x3\t\x3\x4\x4\t\x4\x4\x5\t\x5\x4\x6\t\x6\x4\a\t\a\x4\b\t\b\x4\t"+
		"\t\t\x4\n\t\n\x3\x2\x3\x2\x3\x2\x3\x3\x3\x3\x3\x4\x3\x4\x3\x5\x3\x5\x3"+
		"\x6\x3\x6\x3\a\x3\a\x3\a\x3\a\x3\a\x3\a\x3\b\x3\b\x3\b\x3\b\x3\b\x3\b"+
		"\x3\b\x3\b\x3\t\x6\t\x30\n\t\r\t\xE\t\x31\x3\n\x6\n\x35\n\n\r\n\xE\n\x36"+
		"\x3\n\x3\n\x2\x2\x2\v\x3\x2\x3\x1\x5\x2\x4\x1\a\x2\x5\x1\t\x2\x6\x1\v"+
		"\x2\a\x1\r\x2\b\x1\xF\x2\t\x1\x11\x2\n\x1\x13\x2\v\x2\x3\x2\x4\x5\x2\x32"+
		";\x43\\\x63|\x5\x2\v\f\xF\xF\"\";\x2\x3\x3\x2\x2\x2\x2\x5\x3\x2\x2\x2"+
		"\x2\a\x3\x2\x2\x2\x2\t\x3\x2\x2\x2\x2\v\x3\x2\x2\x2\x2\r\x3\x2\x2\x2\x2"+
		"\xF\x3\x2\x2\x2\x2\x11\x3\x2\x2\x2\x2\x13\x3\x2\x2\x2\x3\x15\x3\x2\x2"+
		"\x2\x5\x18\x3\x2\x2\x2\a\x1A\x3\x2\x2\x2\t\x1C\x3\x2\x2\x2\v\x1E\x3\x2"+
		"\x2\x2\r \x3\x2\x2\x2\xF&\x3\x2\x2\x2\x11/\x3\x2\x2\x2\x13\x34\x3\x2\x2"+
		"\x2\x15\x16\a/\x2\x2\x16\x17\a@\x2\x2\x17\x4\x3\x2\x2\x2\x18\x19\a}\x2"+
		"\x2\x19\x6\x3\x2\x2\x2\x1A\x1B\a\x31\x2\x2\x1B\b\x3\x2\x2\x2\x1C\x1D\a"+
		"\x7F\x2\x2\x1D\n\x3\x2\x2\x2\x1E\x1F\a=\x2\x2\x1F\f\x3\x2\x2\x2 !\au\x2"+
		"\x2!\"\av\x2\x2\"#\a\x63\x2\x2#$\av\x2\x2$%\ag\x2\x2%\xE\x3\x2\x2\x2&"+
		"\'\ak\x2\x2\'(\ap\x2\x2()\ak\x2\x2)*\av\x2\x2*+\ak\x2\x2+,\a\x63\x2\x2"+
		",-\an\x2\x2-\x10\x3\x2\x2\x2.\x30\t\x2\x2\x2/.\x3\x2\x2\x2\x30\x31\x3"+
		"\x2\x2\x2\x31/\x3\x2\x2\x2\x31\x32\x3\x2\x2\x2\x32\x12\x3\x2\x2\x2\x33"+
		"\x35\t\x3\x2\x2\x34\x33\x3\x2\x2\x2\x35\x36\x3\x2\x2\x2\x36\x34\x3\x2"+
		"\x2\x2\x36\x37\x3\x2\x2\x2\x37\x38\x3\x2\x2\x2\x38\x39\b\n\x2\x2\x39\x14"+
		"\x3\x2\x2\x2\x5\x2\x31\x36";
	public static readonly ATN _ATN =
		ATNSimulator.Deserialize(_serializedATN.ToCharArray());
}
} // namespace FSML
